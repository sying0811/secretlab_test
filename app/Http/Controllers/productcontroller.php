<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class productcontroller extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = [];
        $rdata = json_decode($request->getContent(), true);
        foreach($rdata as $key => $value){
            DB::table('tbl_products')->upsert([
                ['pkey'=>$key,'pname'=>$value, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()]],
                ['pkey'],['pname']
            );
            array_push($arr, ($key.':'.$value));
        }
        return ($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = DB::table('tbl_products')->select('pname')->where('pkey','=',$id)->get();
        return $result;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_all_record(){
        $result = DB::table('tbl_products')->select('pkey','pname')->orderBy('id')->get();
        return (json_encode($result));
    }
    
}
